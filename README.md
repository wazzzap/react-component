Create a component using the following:

1. ES6 (ECMAScript 2015)
2. LESS
3. React
4. Webpack

For a font use `font awesome` (http://fontawesome.io/):

- http://fontawesome.io/icon/align-justify/
- http://fontawesome.io/icon/eraser/
- http://fontawesome.io/icon/caret-down/
- http://fontawesome.io/icon/caret-up/

# Component spec

- Default empty state of component has:
     - `input` element for user input with placeholder;
     - drop menu icon for toggle multiline mode;

     ![Default empty state](empty.png)

- Multiline empty state of component has:
     - `texarea` element for user input with placeholder;
     - drop menu icon for toggle singleline mode;

     ![Multiline empty state](empty-multiline.png)

- After user input:
    - Erase action icon is shown;

    ![User Input](user-input.png)

- Other interactions:
    - Toggling between multiline/singleline modes always works;
    - Mouse over style:

        ![Hover](hover.png)

    - Focused style:

        ![Focus](focus.png)

    - Read-only and disabled states has no erase option;
    - Read-only style:

        ![Read Only](read-only.png)

    - Disabled style:

        ![Disabled](disabled.png)

# Result

A repo (github, bitbucket, gitlab) with final project.